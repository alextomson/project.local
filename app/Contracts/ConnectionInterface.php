<?php

namespace app\Contracts;

interface ConnectionInterface
{
    public function __construct(DbConnectionInterface $connect);

    public function connection();

    public function disconnect();

    public function getType();

    public static function getInstance();
}