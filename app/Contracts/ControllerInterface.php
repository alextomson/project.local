<?php

namespace app\Contracts;

interface ControllerInterface
{
    public function setModel(ModelInterface $model);

    public function getModel();
}
