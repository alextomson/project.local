<?php

namespace app\Contracts;

interface DbConnectionInterface
{
    public function __construct(array $dataConnection);

    public function checkDataConnection();

    public function connection();

    public function disconnect();

    public function getType();
}