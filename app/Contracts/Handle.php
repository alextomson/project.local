<?php

namespace app\Contracts;

interface Handle
{
    public function handle();
}
