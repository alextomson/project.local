<?php

namespace app\Contracts;

interface QueryExecutioner
{
    public function select(BuilderInterface $builder);
    public function update(BuilderInterface $builder);
    public function insert(BuilderInterface $builder);
}
