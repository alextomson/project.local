<?php

namespace app\Core;

use app\Contracts\ControllerInterface;
use app\Contracts\ModelInterface;

/**
 * Class Controller
 * @package app\Core
 */
class Controller implements ControllerInterface
{

    public $model;
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }

    public function redirect($url)
    {
        header("location: {$url}");
    }

    public function setModel(ModelInterface $model)
    {
        $this->model = $model;
    }

    public function getModel()
    {
        return $this->model;
    }
}