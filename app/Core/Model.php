<?php

namespace app\Core;

use app\Contracts\ModelInterface;
use app\Contracts\QueryExecutioner;

/**
 * Class Model
 * @package app\Core
 */
class Model implements ModelInterface
{
    public $queryExecutioner;

    public function __construct(QueryExecutioner $queryExecutioner)
    {
        $this->queryExecutioner = $queryExecutioner;
    }
}