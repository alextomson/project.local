<?php

namespace app\Core;

use app\Service\Connection;

/**
 * Class Route
 * @package app\Core
 */
class Route
{
    public static function start()
    {
        $controllerName = 'Task';
        $actionName = 'index';

        $routes = explode('/', $_SERVER['REQUEST_URI']);

        if (!empty($routes[count($routes) - 2])) {
            $controllerName = $routes[count($routes) - 2];
        }

        if (!empty($routes[count($routes) - 1])) {
            $actionName = $routes[count($routes) - 1];

            if (preg_match('/-/', $actionName)) {
                $actionName = str_replace('-', '', ucwords($actionName, '-'));
                $actionName = lcfirst($actionName);
            }

            if (($position = strpos($actionName, '?')) !== FALSE) {
                $actionName = substr($actionName, 0, $position);
            }
        }

        $controllerName = ucfirst(strtolower($controllerName)) . 'Controller';
        $controllerPath = 'app\\Http\\Controllers\\' . $controllerName;
        $connection = Connection::getInstance();

        $queryExecutionerClass = 'app\\Service\\' . ucfirst(strtolower($connection->getType())) . 'QueryExecutioner';
        $queryExecutioner = new $queryExecutionerClass();

        if (class_exists($controllerPath)) {
            $controller = new $controllerPath();
            $modelClass = $controller->getModel();
            $model = new $modelClass($queryExecutioner);
            $controller->setModel($model);
        } else {
            Route::ErrorPage404();
        }

        if (method_exists($controller, $actionName)) {
            $controller->$actionName();
        } else {
            Route::ErrorPage404();
        }
    }

    public static function ErrorPage404()
    {
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header("Status: 404 Not Found");
        header('Location:' . $host . '404');
    }
}
