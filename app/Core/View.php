<?php

namespace app\Core;
/**
 * Class View
 * @package app\Core
 */
class View
{
    public function generate($content_view, $template_view, $data = null)
    {
        if (is_array($data)) {
            extract($data);
        }

        include __DIR__. '/../../resources/views/' . $template_view;
    }
}
