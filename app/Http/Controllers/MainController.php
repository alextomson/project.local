<?php

namespace app\Http\Controllers;

use app\Core\Controller;
use app\Models\User;

/**
 * Class MainController
 * @package app\Http\Controllers
 */
class MainController extends Controller
{
    public function index()
    {
        $this->view->generate('main.php', 'template.php');
    }

    public function login()
    {
        if ($this->model->validate()) {
            $user = array_shift($this->model->searchUser());

            if ($user) {
                session_start();
                $_SESSION['user_id'] = $user['id'];
                parent::redirect('/task/cabinet');
            } else {
                $this->view->generate('main.php', 'template.php', ['error' => 'This user is not exist']);
            }
        } else {
            $this->view->generate('main.php', 'template.php', $this->model->errors);
        }
    }

    public function logout()
    {
        session_start();
        unset($_SESSION['user_id']);

        $this->view->generate('main.php', 'template.php');
    }

    public function getModel()
    {
        return User::class;
    }
}
