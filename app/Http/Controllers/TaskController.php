<?php

namespace app\Http\Controllers;

use app\Core\Controller;
use app\Models\Task;

/**
 * Class TaskController
 * @package app\Http\Controllers
 */
class TaskController extends Controller
{
    public function index()
    {
        session_start();
        $tasks = $this->model->getAllTasks(3);

        $this->view->generate('tasks.php', 'template.php', $tasks);
    }

    public function cabinet()
    {
        session_start();

        if (isset($_SESSION['user_id'])) {
            $tasks = $this->model->getAllTasks();
            $this->view->generate('cabinet.php', 'template.php', $tasks);
        } else {
            parent::redirect('/main/');
        }
    }

    public function updateTask()
    {
        session_start();

        if (isset($_SESSION['user_id'])) {
            $tasks = $this->model->searchTask($_GET['id']);
            $task = array_shift($tasks);
            $this->view->generate('update-task.php', 'template.php', $task);
        } else {
            parent::redirect('/main/');
        }
    }

    public function update()
    {
        session_start();

        if (isset($_SESSION['user_id'])) {
            $_SESSION['success_update'] = true;
            $tasks = $this->model->update();
            parent::redirect('/task/cabinet');
        } else {
            parent::redirect('/main/');
        }
    }

    public function createTask()
    {
        $this->view->generate('create-task.php', 'template.php');
    }

    public function save()
    {
        if ($this->model->validate()) {
            session_start();
            $_SESSION['success_insert'] = true;
            $this->model->insertTask();
            parent::redirect('/task/');
        } else {
            $this->view->generate('create-task.php', 'template.php', $this->model->errors);
        }
    }

    public function ajaxSelect()
    {
        $tasks = $this->model->getAllTasks(3);

        foreach ($tasks['items'] as &$task) {
            $task['status'] = \app\Models\Task::getStatuses()[$task['status']];
            $task['text'] = htmlspecialchars($task['text']);
            $task['email'] = htmlspecialchars($task['email']);
            $task['name'] = htmlspecialchars($task['name']);
        }

        echo json_encode($tasks);
    }

    public function getModel()
    {
        return Task::class;
    }
}
