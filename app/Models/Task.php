<?php

namespace app\Models;

use app\Core\Model;
use app\Service\BuilderService;

/**
 * Class Task
 * @package app\Models
 */
class Task extends Model
{
    public $errors;

    /**
     * @return string
     */
    public static function getTable()
    {
        return 'tasks';
    }

    public const CREATED = 'created';
    public const DONE = 'done';
    public const EDITBYADMIN = 'edit by admin';

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [self::CREATED, self::DONE];
    }

    /**
     * @return mixed
     */
    public function searchTask()
    {
        $builder = new BuilderService();
        $builder->setTable(self::getTable());
        $builder->setConditions($_GET);

        return $this->queryExecutioner->select($builder);
    }

    /**
     * @return mixed
     */
    public function update()
    {
        $builder = new BuilderService();
        $builder->setTable(self::getTable());
        $builder->setConditions(['id' => $_POST['id']]);
        unset($_POST['id']);

        $task = array_shift($this->queryExecutioner->select($builder));

        if ($_POST['text'] != $task['text']) {
            $builder->setAttributes(array_merge($_POST, ['edit_by_admin' => 1]));
        } else {
            $builder->setAttributes($_POST);
        }

        return $this->queryExecutioner->update($builder);
    }

    /**
     * @return mixed
     */
    public function insertTask()
    {
        $builder = new BuilderService();
        $builder->setTable(self::getTable());
        $builder->setAttributes($_POST);

        return $this->queryExecutioner->insert($builder);
    }

    /**
     * @param int $limit
     * @return array
     */
    public function getAllTasks($limit = 0)
    {
        $builder = new BuilderService();
        $builder->setTable(self::getTable());

        $count = count($this->queryExecutioner->select($builder));
        $builder->setLimit($limit);

        if (isset($_GET['to'])) {
            $builder->setOffset(($_GET['to'] - 1) * 3);
        }

        $items = $this->queryExecutioner->select($builder);

        return ['items' => $items, 'count' => $count];
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $attributes = $_POST;
        $success = true;

        if (!$attributes['name']) {
            $this->errors['name'] = 'Required not empty value';
            $success = false;
        }

        if (!$attributes['text']) {
            $this->errors['text'] = 'Required not empty value';
            $success = false;
        }

        if (!preg_match("/^(?:[a-z0-9]+(?:[-_.]?[a-z0-9]+)?@[a-z0-9_.-]+(?:\.?[a-z0-9]+)?\.[a-z]{2,5})$/i", $attributes['email'])) {
            $this->errors['email'] = 'Not correct email';
            $success = false;
        }

        return $success;
    }
}
