<?php

namespace app\Models;

use app\Core\Model;
use app\Service\BuilderService;

/**
 * Class User
 * @package app\Models
 */
class User extends Model
{
    public $errors;

    /**
     * @return string
     */
    public static function getTable()
    {
        return 'users';
    }

    /**
     * @return mixed
     */
    public function searchUser()
    {
        $builder = new BuilderService();
        $builder->setTable(self::getTable());
        $builder->setAttributes(['login' => $_POST['login'], 'password' => md5($_POST['password'])]);

        return $this->queryExecutioner->select($builder);
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $attributes = $_POST;
        $success = true;

        if (!$attributes['login']) {
            $this->errors['login'] = 'Required not empty value';
            $success = false;
        }

        if (!$attributes['password']) {
            $this->errors['password'] = 'Required not empty value';
            $success = false;
        }

        return $success;
    }
}
