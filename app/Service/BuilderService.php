<?php

namespace app\Service;

use app\Contracts\BuilderInterface;

/**
 * Class BuilderService
 * @package app\Service
 */
class BuilderService implements BuilderInterface
{
    private $attributes = [];

    private $conditions = [];

    private $table;

    private $limit = 0;

    private $offset = 0;

    private $sorts = [];

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
    }

    /**
     * @param array $conditions
     */
    public function setConditions(array $conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * @param string $table
     */
    public function setTable(string $table)
    {
        $this->table = $table;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return ' ' . $this->table . ' ';
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return string
     */
    public function getLimit()
    {
        return ' limit ' . $this->limit;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return string
     */
    public function getOffset()
    {
        return ' offset ' . $this->offset;
    }

    /**
     * @param array $sorts
     */
    public function setSort(array $sorts)
    {
        $this->sorts = $sorts;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        $sortLine = '';
        $total = count($this->sorts);
        $counter = 0;

        foreach ($this->sorts as $key => $sort) {
            $counter++;

            if ($counter == $total) {
                $sortLine .= " {$key} {$sort} ";
            } else {
                $sortLine .= " {$key} {$sort} , ";
            }
        }

        return ' order by ' . $sortLine;
    }

    /**
     * @return string
     */
    public function getKeys()
    {
        $keys = array_keys($this->attributes);
        $keyLine = '';
        $total = count($this->attributes);
        $counter = 0;

        foreach ($keys as $key) {
            $counter++;

            if ($counter == $total) {
                $keyLine .= "$key ";
            } else {
                $keyLine .= "$key, ";
            }
        }

        return $keyLine;
    }

    /**
     * @return string
     */
    public function getBindKeys()
    {
        $keys = array_keys($this->conditions);
        $where = '';
        $total = count($this->conditions);
        $counter = 0;

        foreach ($keys as $key) {
            $counter++;

            if ($counter == $total) {
                $where .= " $key = ? ";
            } else {
                $where .= " $key = ? and ";
            }
        }

        return $where;
    }

    /**
     * @return string
     */
    public function getKeysWithBindValue()
    {
        $keysWithValue = '';
        $total = count($this->attributes);
        $counter = 0;

        foreach ($this->attributes as $key => $attribute) {
            $counter++;

            if ($counter == $total) {
                $keysWithValue .= " $key = ? ";
            } else {
                $keysWithValue .= " $key = ?, ";
            }
        }

        return $keysWithValue;
    }

    /**
     * @return string
     */
    public function getConditionTypes()
    {
        $types = '';

        foreach ($this->conditions as $condition) {
            $types .= substr(gettype($condition), 0, 1);
        }

        return $types;
    }

    /**
     * @return string
     */
    public function getAttributeTypes()
    {
        $types = '';

        foreach ($this->attributes as $attribute) {
            $types .= substr(gettype($attribute), 0, 1);
        }

        return $types;
    }

    /**
     * @return array
     */
    public function getBindAttributeParams()
    {
        $params[] = $this->getAttributeTypes();

        foreach ($this->attributes as $attribute) {
            $params[] = $attribute;
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getBindConditionParams()
    {
        $params[] = $this->getConditionTypes();

        foreach ($this->conditions as $condition) {
            $params[] = $condition;
        }

        return $params;
    }

    /**
     * @return array
     */
    public function getBindAttributeAndConditionParams()
    {
        $params[] = $this->getAttributeTypes() . $this->getConditionTypes();

        foreach (array_merge($this->attributes, $this->conditions) as $param) {
            $params[] = $param;
        }

        return $params;
    }

    /**
     * @return string
     */
    public function getCountValues()
    {
        $valueLine = '';
        $total = count($this->attributes);
        $counter = 0;

        foreach ($this->attributes as $attribute) {
            $counter++;

            if ($counter == $total) {
                $valueLine .= "? ";
            } else {
                $valueLine .= "?, ";
            }
        }

        return $valueLine;
    }

    /**
     * @return bool
     */
    public function hasLimit()
    {
        return (bool)$this->limit;
    }

    /**
     * @return bool
     */
    public function hasSort()
    {
        return !empty($this->sorts);
    }

    /**
     * @return bool
     */
    public function hasConditions()
    {
        return !empty($this->conditions);
    }

    /**
     * @return bool
     */
    public function hasOffset()
    {
        return (bool)$this->offset;
    }
}
