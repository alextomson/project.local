<?php

namespace app\Service;

use app\Contracts\ConnectionInterface;
use app\Contracts\DbConnectionInterface;

/**
 * Class Connection
 * @package app\Service
 */
class Connection implements ConnectionInterface
{
    protected $connection;
    private static $instance = null;

    /**
     * Connection constructor.
     * @param DbConnectionInterface $connect
     */
    public function __construct(DbConnectionInterface $connect)
    {
         $this->connection = $connect;
         self::$instance = $this;
    }

    /**
     * @return mixed
     */
    public function connection()
    {
        return $this->connection->connection();
    }

    public function disconnect()
    {
        $this->connection->disconnect();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->connection->getType();
    }

    /**
     * @return Connection|null
     */
    public static function getInstance()
    {
        if (self::$instance != null) {
            return self::$instance;
        }

        return new self;
    }
}
