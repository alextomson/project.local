<?php

namespace app\Service\DbConnection;

use app\Contracts\DbConnectionInterface;

/**
 * Class MySqlConnection
 * @package app\Service\DbConnection
 */
class MySqlConnection implements DbConnectionInterface
{
    protected $dataConnection;
    protected $connection;

    /**
     * MySqlConnection constructor.
     * @param array $dataConnection
     */
    public function __construct(array $dataConnection)
    {
        $this->dataConnection = $dataConnection;
    }

    /**
     * @return bool
     */
    public function checkDataConnection(): bool
    {
         if (array_key_exists('host', $this->dataConnection) &&
             array_key_exists('password', $this->dataConnection) &&
             array_key_exists('database', $this->dataConnection) &&
             array_key_exists('user', $this->dataConnection))
         {
             return true;
         }
         else {
             return false;
         }
    }

    /**
     * @return false|\mysqli
     * @throws \Exception
     */
    public function connection()
    {
        if ($this->checkDataConnection()) {
            $this->connection = mysqli_connect($this->dataConnection['host'], $this->dataConnection['user'],
                $this->dataConnection['password'], $this->dataConnection['database'])
            or die("Ошибка " . mysqli_error($this->connection));

            return $this->connection;
        }
        else {
            throw new \Exception('Need add information for ' . static::class);
        }
    }

    public function disconnect()
    {
        mysqli_close($this->connection);
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'Mysql';
    }
}
