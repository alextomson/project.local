<?php

namespace app\Service;
/**
 * Class FileService
 * @package app\Service
 */
class FileService
{
    /**
     * @param $in
     * @return bool|mixed
     */
    function fetchArray($in)
    {
        if (is_file($in))
            return include $in;

        return false;
    }
}
