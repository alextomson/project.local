<?php

namespace app\Service;

use app\Contracts\Handle;

/**
 * Class InitConnectionHandle
 * @package app\Service
 */
class InitConnectionHandle implements Handle
{
    protected $databaseConfig;
    protected $appConfig;

    /**
     * InitConnectionHandle constructor.
     * @param $databaseConfig
     * @param $appConfig
     */
    public function __construct($databaseConfig, $appConfig)
    {
        $this->databaseConfig = $databaseConfig;
        $this->appConfig = $appConfig;
    }

    /**
     * @return Connection
     */
    public function handle()
    {
        $className = 'app\\Service\\DbConnection\\' .  $this->appConfig['use_database'] . 'Connection';
        $classConnection = new $className($this->databaseConfig[$this->appConfig['use_database']]);

        return new \app\Service\Connection($classConnection);
    }
}
