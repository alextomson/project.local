<?php

namespace app\Service;

use app\Contracts\BuilderInterface;
use app\Contracts\QueryExecutioner;
use app\Service\Traits\Connect;

/**
 * Class MysqlQueryExecutioner
 * @package app\Service
 */
class MysqlQueryExecutioner implements QueryExecutioner
{
    use Connect;

    /**
     * @param BuilderInterface $builder
     * @return |null
     * @throws \ReflectionException
     */
    public function update(BuilderInterface $builder)
    {
        $mysqli = $this->startConnection();
        $sql = 'update ' . $builder->getTable() . ' SET ' . $builder->getKeysWithBindValue() . ' where ' . $builder->getBindKeys();
        $updateRow = null;
        $query = $this->prepareAllParams($mysqli, $builder, $sql);

        if ($query->execute()) {
            $updateId = $query->id;
            $query->close();
            $this->finishConnection();
        }

        if (isset($updateId)) {
            $builder->setConditions(['id' => $updateId]);
            $updateRow = $this->select($builder);
        }

        return $updateRow;
    }

    /**
     * @param BuilderInterface $builder
     * @return |null
     * @throws \ReflectionException
     */
    public function insert(BuilderInterface $builder)
    {
        $mysqli = $this->startConnection();
        $sql = 'insert into ' . $builder->getTable() . '(' . $builder->getKeys() . ') VALUES (' . $builder->getCountValues() . ');';
        $insertRow = null;

        $query = $this->prepareBindAttribute($mysqli, $builder, $sql);

        if ($query->execute()) {
            $insertId = $query->insert_id;
            $query->close();
            $this->finishConnection();
        }

        if (isset($insertId)) {
            $builder->setConditions(['id' => $insertId]);
            $insertRow = $this->select($builder);
        }

        return $insertRow;
    }

    /**
     * @param BuilderInterface $builder
     * @return |null
     * @throws \ReflectionException
     */
    public function select(BuilderInterface $builder)
    {
        $mysqli = $this->startConnection();
        $sql = 'select*from' . $builder->getTable();

        if ($builder->hasConditions()) {
            $sql .= 'where' . $builder->getBindKeys();
            $query = $this->prepareBindCondition($mysqli, $builder, $sql);
        }

        if ($builder->hasSort()) {
            $sql .= $builder->getSort();
        }

        if ($builder->hasLimit()) {
            $sql .= $builder->getLimit();
        }

        if ($builder->hasOffset()) {
            $sql .= $builder->getOffset();
        }

        if (!isset($query)) {
            $query = $mysqli->prepare($sql);
        }

        if ($query->execute()) {
            $result = $query->get_result();
            $rows = $result->fetch_all(MYSQLI_ASSOC);
        } else {
            $rows = null;
        }

        $query->close();
        $this->finishConnection();

        return $rows;
    }

    /**
     * @param $mysqli
     * @param $builder
     * @param $sql
     * @return mixed
     * @throws \ReflectionException
     */
    protected function prepareAllParams($mysqli, $builder, $sql)
    {
        $query = $mysqli->prepare($sql);
        $bindParams = $builder->getBindAttributeAndConditionParams();

        $ref = new \ReflectionClass('mysqli_stmt');
        $method = $ref->getMethod("bind_param");
        $method->invokeArgs($query, $this->refValues($bindParams));

        return $query;
    }

    /**
     * @param $mysqli
     * @param $builder
     * @param $sql
     * @return mixed
     * @throws \ReflectionException
     */
    protected function prepareBindAttribute($mysqli, $builder, $sql)
    {
        $query = $mysqli->prepare($sql);
        $bindParams = $builder->getBindAttributeParams();


        $ref = new \ReflectionClass('mysqli_stmt');
        $method = $ref->getMethod("bind_param");
        $method->invokeArgs($query, $this->refValues($bindParams));

        return $query;
    }

    /**
     * @param $mysqli
     * @param $builder
     * @param $sql
     * @return mixed
     * @throws \ReflectionException
     */
    protected function prepareBindCondition($mysqli, $builder, $sql)
    {
        $query = $mysqli->prepare($sql);
        $bindParams = $builder->getBindConditionParams();


        $ref = new \ReflectionClass('mysqli_stmt');
        $method = $ref->getMethod("bind_param");
        $method->invokeArgs($query, $this->refValues($bindParams));

        return $query;
    }

    /**
     * @param $arr
     * @return array
     */
    private function refValues($arr)
    {
        if (strnatcmp(phpversion(), '5.3') >= 0) {
            $refs = array();

            foreach ($arr as $key => $value)
                $refs[$key] = &$arr[$key];

            return $refs;
        }

        return $arr;
    }
}