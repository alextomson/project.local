<?php

namespace app\Service\Traits;

use app\Service\Connection;

/**
 * Trait Connect
 * @package app\Service\Traits
 */
trait Connect
{
    public $connection;

    function startConnection()
    {
        $this->connection = Connection::getInstance();
        return $this->connection->connection();
    }

    function finishConnection()
    {
        $this->connection->disconnect();
    }
}
