<?php

$fileService = new \app\Service\FileService();

$appConfig = $fileService->fetchArray(__DIR__ . '/../config/app.php');
$databaseConfig = $fileService->fetchArray(__DIR__ . '/../config/database.php');

if (array_key_exists('use_database', $appConfig))
{
    $connection = (new app\Service\InitConnectionHandle($databaseConfig, $appConfig))->handle();
    $connection->connection();
    $connection->disconnect();
}