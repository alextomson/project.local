function preSort($attr)
{
    if (!$attr.attr( "data-order" )) {
        $attr.attr('data-order', 'asc');
        $attr.text($attr.attr('data-name') + "↓")
    } else if ($attr.attr( "data-order" ) == 'asc') {
        $attr.attr('data-order', 'desc');
        $attr.text($attr.attr('data-name') + "↑")
    } else {
        $attr.attr('data-order', '');
        $attr.text($attr.attr('data-name'));
        $to = parseInt($(".current").text());
        updateTable($to);

        return false;
    }

    return true;
}

function updateTable($to) {
    $.ajax({
        url: 'task/ajax-select',
        data: "to=" + $to,
        contentType: "json",
        type: 'get',
        success: function (response) {
            var returnedData = JSON.parse(response);
            $(".tbody tr").each(function (index) {
                $(this).remove();
            });

            $.each(returnedData.items, function (i, $value) {
                $('.tbody').append($('<tr>')
                    .append($('<td class="name">').append($value.name))
                    .append($('<td class="email">').append($value.email))
                    .append($('<td class="text">').append($value.text))
                    .append($('<td class="status">').append($value.status))
                );

            });
            initialSort();
        }
    });
}

function initialSort()
{
    $(".task a").each(function(){
        $attr = $(this);

        if ($attr.attr( "data-order" )){
            sort($attr.attr('data-name'), $attr.attr( "data-order" ));
        }
    });
}

function sort($name, $sort)
{
    tbody = $(".tbody");
    tbody.find('tr').sort(function(a, b)
    {
        if($sort == 'asc')
        {
            return $('td.'+ $name, a).text().localeCompare($('td.' + $name, b).text());
        }
        else if ($sort == 'desc')
        {
            return $('td.'+ $name, b).text().localeCompare($('td.' + $name, a).text());
        }
    }).appendTo(tbody);
}