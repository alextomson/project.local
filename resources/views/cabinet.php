<?php

if (isset($_SESSION) && array_key_exists('success_update', $_SESSION) && $_SESSION['success_update']):?>
    <div class="alert alert-success" role="alert">
        Success update task!
    </div>
<?php endif;?>

<a class = 'btn btn-primary' href="/main/logout">Logout</a>
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">Email</th>
        <th scope="col">Text</th>
        <th scope="col">Status</th>
        <th scope="col">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data['items'] as $datum):?>
        <tr>
            <th><?=htmlspecialchars($datum['name'])?></th>
            <td><?=htmlspecialchars($datum['email'])?></td>
            <td><?=htmlspecialchars($datum['text'])?></td>
            <td>
                <?=\app\Models\Task::getStatuses()[$datum['status']]?>;
                <?php if ($datum['edit_by_admin']):?>
                    <?=\app\Models\Task::EDITBYADMIN?>
                <?php endif;?>
            </td>
            <td><a class = 'btn btn-primary' href="/task/update-task?id=<?=$datum['id']?>">Update task</a></td>
        </tr>

    <?php endforeach;?>
    </tbody>
</table>
