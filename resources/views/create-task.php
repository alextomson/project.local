<div class="row d-flex justify-content-center">
    <!--Grid column-->
    <div class="col-md-6 margin-top-40">

        <form action= "/task/save" method="post">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8 col-sm-12 form-group">
                            <label for="Name">Name</label>
                            <input type="text" class="form-control <?php if (isset($data['name'])) echo 'is-invalid';?>" id="Name" name="name" placeholder="Name">
                            <div class="invalid-feedback">
                                Required not empty value
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 form-group">
                            <label for="Text">Text</label>
                            <textarea class="form-control <?php if (isset($data['text'])) echo 'is-invalid';?>" aria-label="With textarea" id="Text" name="text" placeholder="Text"></textarea>
                            <div class="invalid-feedback">
                                Required not empty value
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12 form-group">
                            <label for="InputEmail">Email</label>
                            <input type="text" class="form-control <?php if (isset($data['email'])) echo 'is-invalid';?>" id="InputEmail" name="email" placeholder="Email">
                            <div class="invalid-feedback">
                                Required not empty value and email type
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mb-2">Save</button>
                    <a class="btn btn-primary mb-2" href="/task/index">Prev</a>
                </div>
            </div>
        </div>
        </form>

    </div>
    <!--Grid column-->

</div>