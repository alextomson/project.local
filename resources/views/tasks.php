<?php

$total = intval(($data['count'] - 1) / 3) + 1;

if (empty($page) or $page < 0) $page = 1;

if ($page > $total) $page = $total;

$page = intval($page);

if (isset($_SESSION) && array_key_exists('success_insert', $_SESSION) && $_SESSION['success_insert']):?>
    <div class="alert alert-success" role="alert">
        Success insert task!
    </div>
<?php endif; ?>
<a class='btn btn-primary' href="/task/create-task">Create task</a>

<a class='btn btn-primary' href="/main/">Login</a>
<table class="table">
    <thead class="thead-dark">
    <tr>
        <th scope="col" class="task"><a data-name="name">name</a></th>
        <th scope="col" class="task"><a data-name="email">email</a></th>
        <th scope="col" class="task"><a data-name="text">text</a></th>
        <th scope="col" class="task"><a data-name="status">status</a></th>
    </tr>
    </thead>
    <tbody class="tbody">
    <?php foreach ($data['items'] as $datum): ?>
        <tr>
            <td class="name"><?= htmlspecialchars($datum['name']) ?></td>
            <td class="email"><?= htmlspecialchars($datum['email']) ?></td>
            <td class="text"><?= htmlspecialchars($datum['text']) ?></td>
            <td class="status"><?= \app\Models\Task::getStatuses()[$datum['status']] ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php
$pagination = '';
$pagination .= '<button class="prev-to-one"><<</button><button class="prev-to-minus-one"><</button> ';
$pagination .= ' <button class="prev-to-minus-two">' . ($page - 2) . '</button> | ';
$pagination .= '<button class="prev-to-minus-one-number">' . ($page - 1) . '</button> | ';

$pagination .= '<b class="current">' . $page . '</b>';

$pagination .= ' | <button class="next-to-plus-one-number">' . ($page + 1) . '</button>';

$pagination .= ' | <button class="next-to-plus-two">' . ($page + 2) . '</button>';

$pagination .= ' <button class="next-to-plus-one">></button>
                                   <button class="next-to-end">>></button>';

echo $pagination;

?>

<script>
    $(document).ready(() => {
        updatePagination(<?=$page?>);

        $(".task a").click(function(){
            if (preSort($(this)))
            {
                sort($(this).attr('data-name'), $(this).attr( "data-order" ));
            }
        });

        $(".next-to-plus-one, .next-to-plus-one-number").click(
            function () {
                $to = parseInt($(".current").text()) + 1;
                updateTable($to);
                $(".current").text($to);
                updatePagination($to);
            }
        );
        $(".next-to-plus-two").click(
            function () {
                $to = parseInt($(".current").text()) + 2;
                updateTable($to);
                $(".current").text($to);
                updatePagination($to);
            }
        );
        $(".next-to-end").click(
            function () {
                $to = <?=$total?>;
                updateTable($to);
                $(".current").text($to);
                updatePagination($to);
            }
        );
        $(".prev-to-one").click(
            function () {
                $to = 1;
                updateTable($to);
                $(".current").text($to);
                updatePagination($to);
            }
        );
        $(".prev-to-minus-one, .prev-to-minus-one-number").click(
            function () {
                $to = parseInt($(".current").text()) - 1;
                updateTable($to);
                $(".current").text($to);
                updatePagination($to);
            }
        );
        $(".prev-to-minus-two").click(
            function () {
                $to = parseInt($(".current").text()) - 2;
                updateTable($to);
                $(".current").text($to);
                updatePagination($to);
            }
        );
    });

    function updatePagination($to) {
        if ($to != 1) {
            $(".prev-to-one").css('display', 'inline');
        } else {
            $(".prev-to-one").css('display', 'none');
        }
        if ($to - 1 > 0) {
            $(".prev-to-minus-one, .prev-to-minus-one-number").css('display', 'inline');
            $(".prev-to-minus-one-number").text($to - 1);
        } else {
            $(".prev-to-minus-one, .prev-to-minus-one-number").css('display', 'none');
        }
        if ($to - 2 > 0) {
            $(".prev-to-minus-two").css('display', 'inline');
            $(".prev-to-minus-two").text($to - 2);
        } else {
            $(".prev-to-minus-two").css('display', 'none');
        }
        if ($to + 1 <= <?=$total?>) {
            $(".next-to-plus-one, .next-to-plus-one-number").css('display', 'inline');
            $(".next-to-plus-one-number").text($to + 1);
        } else {
            $(".next-to-plus-one, .next-to-plus-one-number").css('display', 'none');
        }
        if ($to + 2 <= <?=$total?>) {
            $(".next-to-plus-two").css('display', 'inline');
            $(".next-to-plus-two").text($to + 2)
        } else {
            $(".next-to-plus-two").css('display', 'none');
        }
        if ($to != <?=$total?>) {
            $(".next-to-end").css('display', 'inline');
        } else {
            $(".next-to-end").css('display', 'none');
        }

    }
</script>
<script src="/resources/js/script.js"></script>
