<div class="row d-flex justify-content-center">
    <!--Grid column-->
    <div class="col-md-6 margin-top-40">

        <form action="/task/update" method="post">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-md-8">
                        <div class="row">
                            <input type="hidden" name="id" value="<?=$data['id']?>">
                            <div class="col-md-8 col-sm-12 form-group">
                                <label for="Name">Name</label>
                                <input type="text" value='<?php echo $data['name'] ?? ''; ?>' class="form-control"
                                       id="Name" name="name" placeholder="Name">
                            </div>
                            <div class="col-md-8 col-sm-12 form-group">
                                <label for="Text">Text</label>
                                <textarea class="form-control" aria-label="With textarea" id="Text" name="text"
                                          placeholder="Text"><?= $data['text'] ?></textarea>
                            </div>
                            <div class="col-md-8 col-sm-12 form-group">
                                <label for="InputEmail">Email</label>
                                <input type="text" value='<?php echo $data['email'] ?? ''; ?>' class="form-control"
                                       id="InputEmail" name="email" placeholder="Email">
                            </div>
                            <div class="col-md-8 col-sm-12 form-group">
                                <select class="form-control" name="status">
                                    <option value="0" <?php if ($data['status'] == 0): ?> selected<?php endif; ?>>
                                        Created
                                    </option>
                                    <option value="1" <?php if ($data['status'] == 1): ?> selected<?php endif; ?>>Done
                                    </option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mb-2">Save</button>
                        <a class="btn btn-primary mb-2" href="/task/cabinet">Prev</a>
                    </div>
                </div>
            </div>
        </form>

    </div>
    <!--Grid column-->

</div>